import spack
import os
import csv
import glob

version = "LCG_101"

def parse_lcg(directory=version):
  lcg_dict = {}
  for fn in glob.glob(os.path.join(directory, "LCG_*.txt")):
    if 'contrib' in fn: 
      continue
    print(fn)
    with open(fn, newline='') as csvfile:
      csvreader = csv.reader(csvfile, delimiter=';')
      # skip header, this is usually something like
      #PLATFORM: x86_64-centos7-clang10-dbg
      #VERSION: 99
      #COMPILER: clang, 10.0.0
      next(csvreader)
      next(csvreader)
      next(csvreader)
      package_blacklist = ['graphviz', 'hive', 'pkgconfig', 'ipython-genutils', 'snappy']
      for row in csvreader:
              lcg_dict[row[0]] = row[2]
              # skip packages which will be installed explicitly
              if lcg_dict[row[0]] in package_blacklist:
                next(csvreader)
  return lcg_dict

def parse_translator():
  with open('lcgmake_spack_translator.csv', newline='') as csvfile:
    packagename_dict = {}
    rdr = csv.reader(filter(lambda row: row[0]!='#', csvfile))
    for row in rdr:
          if row[0].strip() and row[1].strip():
            packagename_dict[row[0].strip()] = row[1].strip()
  return packagename_dict


def parse_version_dict():
  version_dict = {}
  # root: remove leading 'v'
  l_root = lambda s: s.replace('v', '')
  version_dict['ROOT'] = l_root 

  # gaudi: remove leading 'v'
  l_gaudi = lambda s: s.replace('v', '').replace('r', '.')
  version_dict['Gaudi'] = l_gaudi

  # garfieldpp: HEAD -> master
  l_garfieldpp = lambda s: s.replace('HEAD', 'master')
  version_dict['Garfield++'] = l_garfieldpp

  # openblas: remove suffic
  l_openblas = lambda s: s.replace(',openblas', '')
  version_dict['blas'] = l_openblas

  # intel_tbb: '.' -> '_U'
  tbb = lambda s: s.replace('.', '_U')
  version_dict['tbb'] = tbb

  # py-pygdal: 3.3.0 -> 3.3.0.10
  pygdal = lambda s: s.replace('3.3.0', '3.3.0.10')
  version_dict['pygdal'] = pygdal

  # sio: e.g. 00.01.00 -> 0.1
  sio = lambda s: s.replace('.0', '.').replace('00', '0').rstrip('.0')
  version_dict['SIO'] = sio

  # hadoop: e.g. 2.7.5.1 -> 2.7.5
  hadoop = lambda s: '.'.join(s.split('.')[:3])
  version_dict['hadoop'] = hadoop

  # py-pycparser: e.g. 2.2 -> 2.20
  pycparser = lambda s: s + '0' if s[-2] == '.' else s
  version_dict['py-pycparser'] = pycparser

  # pythia8: e.g. 8304 -> 8.304
  pythia8 = lambda s: s[0] + '.' + s[1:]
  version_dict['pythia8'] = pythia8
  
  return version_dict

def write_spack_yaml(lcg_dict, packagename_dict, version=version):
  version_dict = parse_version_dict()
  with open('packages_'+version+'.yaml', "w") as f:
    f.write("packages:\n")
    for l in lcg_dict.keys():
      try:
        f.write("  %s:\n" % packagename_dict[l])
        if l in version_dict.keys():
          version = version_dict[l](lcg_dict[l])
        else:
          version = lcg_dict[l]
        f.write("    version: [%s]\n" % version.strip())
      except KeyError:
        print("skipping key ", l, '...')





if __name__ == "__main__":
  lcg_dict = parse_lcg()
  packagename_dict = parse_translator()
  print(packagename_dict)
  write_spack_yaml(lcg_dict, packagename_dict)
