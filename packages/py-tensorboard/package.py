# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyTensorboard(PythonPackage):
    """TensorBoard is a suite of web applications for inspecting and understanding your TensorFlow runs and graphs."""

    homepage = "https://github.com/tensorflow/tensorboard"
    url      = "https://files.pythonhosted.org/packages/f7/fd/67c61276de025801cfa8a1b9af2d7c577e7f27c17b6bff2baca20bf03543/tensorboard-2.8.0-py3-none-any.whl"

    version('2.8.0', sha256='65a338e4424e9079f2604923bdbe301792adce2ace1be68da6b3ddf005170def', expand=False)

    depends_on('python@3.6:3', type=('build', 'run'))
    depends_on('py-wheel@0.26:', type='build')
    depends_on('py-setuptools@41:', type='build')
    depends_on('py-mock', type=('build', 'run'))
    depends_on('py-werkzeug@0.11.15:', type=('build', 'run'))
    depends_on('py-markdown@2.6.8:', type=('build', 'run'))
    depends_on('py-six', type=('build', 'run'))
    depends_on('py-numpy@1.12.0:', type=('build', 'run'))
    depends_on('py-google-auth@1.6.3:2', type=('build', 'run'))
    depends_on('py-google-auth-oauthlib@0.4.1:0.4', type=('build', 'run'))
    depends_on('py-grpcio@1.24.3:', type=('build', 'run'))
    depends_on('py-absl-py@0.4:', type=('build', 'run'))
    depends_on('protobuf@3.6.0:', type=('build', 'run'))
    depends_on('py-requests@2.21.0:2', type=('build', 'run'))
    depends_on('py-tensorboard-data-server@0.6.0:0.6', type=('build', 'run'))
    depends_on('py-tensorboard-plugin-wit@1.6.0:', type=('build', 'run'))

    extends('python')

    parallel = False
