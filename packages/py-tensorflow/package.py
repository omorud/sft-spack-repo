# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import sys


class PyTensorflow(PythonPackage):
    """TensorFlow is an open source software library
    for high performance numerical computation."""

    homepage = "https://www.tensorflow.org"

    if sys.platform == 'darwin':
        version('2.8.0', sha256='52f225fecc688281b3ae2cba2b52d3ed6215ed4a3ffb686b9cfd09885ca65563',
                url='https://files.pythonhosted.org/packages/80/26/2990ee24ea5ef5ece557828ce295abc75e5b6c0c8ebf571a842f6e1125db/tensorflow-2.8.0-cp39-cp39-macosx_10_14_x86_64.whl',
                expand=False)
    elif sys.platform.startswith('linux'):
        version('2.8.0', sha256='9d91a989e5455ae713c03fd7236071ab3f232ad8ff2831f2658072933546091f',
                url='https://files.pythonhosted.org/packages/b9/39/88799b7257f73482d4c77bc18c068902a91629c6d380db88cc28ede44fd5/tensorflow-2.8.0-cp39-cp39-manylinux2010_x86_64.whl',
                expand=False)

    depends_on('python@3.9', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-absl-py@0.13.0:', type=('build', 'run'))
    depends_on('py-astunparse@1.6.3:', type=('build', 'run'))
    depends_on('py-flatbuffers@2.0:', type=('build', 'run'))
    depends_on('py-google-pasta@0.2.0:', type=('build', 'run'))
    depends_on('py-h5py@3.6.0:', type=('build', 'run'))
    depends_on('py-keras-preprocessing@1.1.2:', type=('build', 'run'))
    depends_on('py-numpy@1.21.4:', type=('build', 'run'))
    depends_on('py-opt-einsum@3.3.0:', type=('build', 'run'))
    depends_on('py-protobuf@3.19.3:', type=('build', 'run'))
    depends_on('py-six@1.16.0:', type=('build', 'run'))
    depends_on('py-termcolor@1.1.0:', type=('build', 'run'))
    depends_on('py-typing-extensions@3.10.0.0:', type=('build', 'run'))
    depends_on('py-wheel@0.36.2:', type=('build', 'run'))
    depends_on('py-wrapt@1.12.1:', type=('build', 'run'))
    depends_on('py-gast@0.4.0', type=('build', 'run'))
    depends_on('py-tensorboard@2.8.0', type=('build', 'run'))

    extends('python')

    variant('c', default=True, description='Build with C support')

    depends_on('libtensorflow', when='+c')

    @when('+c')
    @run_after('install')
    def add_libtensorflow(self):
        install_tree(self.spec['libtensorflow'].prefix.lib, self.spec.prefix.lib)
        install_tree(self.spec['libtensorflow'].prefix.include, self.spec.prefix.include)
