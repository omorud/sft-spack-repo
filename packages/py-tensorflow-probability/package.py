# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyTensorflowProbability(PythonPackage):
    """TensorFlow Probability is a library for probabilistic
    reasoning and statistical analysis in TensorFlow."""

    homepage = "https://github.com/tensorflow/probability"
    url      = "https://files.pythonhosted.org/packages/f9/e8/f3901a9296912c7d67f65063d41fc16dd981e48131089e3f7acda9389252/tensorflow_probability-0.16.0-py2.py3-none-any.whl"

    version('0.16.0', sha256='48bcaa772ebbfa535594df025b3d7a5f531753117fd78f01bf740667dace99f7', expand=False)

    depends_on('py-setuptools', type='build')
    depends_on('py-absl-py', type=('build', 'run'))
    depends_on('py-six@1.10.0:', type=('build', 'run'))
    depends_on('py-numpy@1.13.3:', type=('build', 'run'))
    depends_on('py-decorator', type=('build', 'run'))
    depends_on('py-cloudpickle@1.3:', type=('build', 'run'))
    depends_on('py-gast@0.3.2:', type=('build', 'run'))
    depends_on('py-dm-tree', type=('build', 'run'))
    depends_on('py-tensorflow', type=('build', 'run'))

    extends('python')
