# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from datetime import datetime
from spack.pkg.lcg.SetupScriptPackage import SetupScriptPackage


class LcgRelease(BundlePackage, SetupScriptPackage):
    """Bundle package for full LCG release"""

    homepage = "https://gitlab.cern.ch/sft/lcgcmake"

    maintainers = ['razumov', 'ganis', 'dkonst', 'vavolkl']

    ##################### versions ########################
    #######################################################
    ###  nightly builds
    # builds the HEAD of each package for which 
    # k4_add_latest_commit_as_dependency is called below
    version('master')
    # this version can be extended with additional version
    # fields to differentiate it, like 'master-2020-10-10'
    #
    ### stable builds
    # builds the latest release of each package
    # the preferred usage is to use the date as version, like:
    version(datetime.today().strftime('%Y-%m-%d'))
    #version('2020-10-06') # example, no need to add here.
    # more complex version configurations (like the usual version numbering ) 
    # should be added in an environment

    # this bundle package installs a custom setup script, so
    # need to add the install phase (which normally does not
    # exist for a bundle package)
    phases = ['install']

    variant('layer', default='python3_ATLAS_5', values=('python3_ATLAS_5',), multi=False, description='Layer to build')



    depends_on('cuda')
    depends_on('clingo-bootstrap@spack')

    ### database 
    depends_on('hbase')
    #oracle
    depends_on('postgresql')
    depends_on('sqlite')
    depends_on('mariadb')

    ### graphics
    depends_on('cairo')
    depends_on('coin3d')
    depends_on('py-cycler')
    depends_on('fontconfig')
    depends_on('py-cartopy')
    depends_on('cfitsio')
    depends_on('gdal')
    depends_on('gl2ps')
    depends_on('gnuplot')
    depends_on('graphviz')
    depends_on('harfbuzz')
    #depends_on('imagemagick') # disabled to avoid huge ghostscript dependencies
    depends_on('py-matplotlib')
    depends_on('pango')
    depends_on('pixman')
    depends_on('libpng')
    depends_on('py-pycairo')
    depends_on('py-pydot')
    depends_on('py-pygdal')
    depends_on('py-graphviz')
    depends_on('py-pyproj')
    depends_on('qt')
    depends_on('py-seaborn')
    depends_on('libtiff')


    ### IO
    depends_on('py-astor')
    depends_on('py-atomicwrites')
    depends_on('cool')
    depends_on('coral')
    depends_on('edm4hep')
    depends_on('flatbuffers')
    depends_on('frontier-client')
    depends_on('hdf5')
    depends_on('lcio')
    depends_on('netcdf-c')
    depends_on('podio')
    depends_on('py-pyshp')
    depends_on('py-tables')
    depends_on('sio')
    depends_on('xrootd +krb5')

    ### Math
    depends_on('aida')
    depends_on('openblas')
    depends_on('eigen')
    depends_on('fastjet')
    depends_on('fftw')
    depends_on('geos')
    depends_on('gmp')
    depends_on('gsl')
    depends_on('py-kiwisolver')
    depends_on('libsvm')
    depends_on('mpfi')
    depends_on('mpfr')
    depends_on('proj')
    depends_on('py-sympy')
    depends_on('vc')
    depends_on('vecgeom')


    ### XML
    depends_on('py-defusedxml')
    depends_on('expat')
    depends_on('libxml2')
    depends_on('libxslt')
    depends_on('py-lxml')
    depends_on('xerces-c')
    depends_on('xqilla')

    ### Simulation
    depends_on('delphes')
    depends_on('geant4')
    depends_on('hepmc')
    depends_on('hepmc3')
    depends_on('heppdt')

    ### Tool
    depends_on('ant')
    depends_on('py-asn1crypto')
    depends_on('py-astroid')
    depends_on('autoconf')
    depends_on('automake')
    depends_on('py-autopep8')
    depends_on('py-backports-abc', when="^python@:3.2")
    depends_on('py-awkward')
    depends_on('py-bcrypt')
    depends_on('benchmark')
    depends_on('py-bleach')
    depends_on('boost')
    depends_on('brotli')
    depends_on('py-cachetools')
    depends_on('ccache')
    depends_on('py-certifi')
    depends_on('py-cffi')
    depends_on('py-chardet')
    depends_on('clhep')
    depends_on('py-click')
    depends_on('py-cloudpickle')
    depends_on('cmake')
    depends_on('hsf-cmaketools')
    #cmmnbuild 	2.1.3
    depends_on('py-coverage')
    depends_on('cppgsl')
    depends_on('cppunit')
    #depends_on('py-cryptography') # temporarily disabled to avoid rust dependency
    depends_on('py-cython')
    depends_on('py-dask')
    depends_on('davix')
    depends_on('py-decorator')
    depends_on('py-dill')
    depends_on('py-distlib')
    depends_on('py-distributed')
    depends_on('double-conversion')
    depends_on('doxygen')
    depends_on('elasticsearch')
    depends_on('py-entrypoints')
    depends_on('fjcontrib')
    depends_on('py-flake8')
    depends_on('fmt')
    depends_on('fplll')
    depends_on('freetype')
    depends_on('ftjam')
    depends_on('py-future')
    depends_on('gdb')
    depends_on('gflags')
    depends_on('glog')
    #go_demangle: Added to LCG in 2019. No release in source code: https://github.com/ianlancetaylor/demangle
    #go_liner: Added to LCG in 2019. No changes in source code since 2015: https://github.com/PLWagner/golang-liner
    #gomacro: Added to LCG in 2019. Last release in 2018: https://github.com/cosmos72/gomacro
    depends_on('py-google-auth')
    depends_on('gperf')
    depends_on('gperftools')
    depends_on('googletest')
    depends_on('py-h5py')
    depends_on('hadoop')
    depends_on('py-heapdict')
    depends_on('py-hepdata-converter')
    depends_on('py-hepdata-validator')
    depends_on('hive')
    depends_on('py-html5lib')
    depends_on('py-idna')
    depends_on('igprof')
    depends_on('py-imageio')
    depends_on('py-ipaddress')
    depends_on('py-ipykernel')
    depends_on('py-ipython-genutils')
    depends_on('py-ipython')
    depends_on('py-ipywidgets')
    depends_on('java')
    depends_on('py-jedi')
    depends_on('jemalloc')
    depends_on('py-jinja2')
    depends_on('py-jpype1')
    depends_on('json-c')
    depends_on('jsoncpp')
    depends_on('nlohmann-json')
    depends_on('py-jsonschema')
    #jupyter_contrib_core 	0.3.3
    #jupyter_contrib_nbextensions 	0.5.1
    #jupyter_nbextensions_configurator 	0.4.1
    depends_on('py-kubernetes')
    depends_on('lcgenv')
    depends_on('lcov')
    depends_on('libaio')
    depends_on('libgit2')
    depends_on('libsodium')
    depends_on('libtool')
    depends_on('libunwind')
    depends_on('py-logilab-common')
    depends_on('lz4')
    depends_on('m4')
    depends_on('py-markupsafe')
    depends_on('maven')
    #messaging 	1.1
    #metakernel: Added to LCG in 2019.
    depends_on('py-minrpc')
    depends_on('py-mistune')
    depends_on('py-mock')
    depends_on('py-msgpack')
    depends_on('py-multiprocess')
    depends_on('py-nbformat')
    depends_on('py-networkx')
    depends_on('ninja')
    #depends_on('node-js')
    depends_on('py-nose')
    depends_on('py-notebook')
    depends_on('py-numexpr')
    depends_on('py-numpy')
    depends_on('py-oauthlib')
    depends_on('ocaml')
    depends_on('py-pandas')
    depends_on('py-pandocfilters')
    depends_on('py-pathlib2')
    depends_on('py-pathos')
    depends_on('pcre')
    depends_on('py-pexpect')
    depends_on('py-pickleshare')
    depends_on('py-pip')
    depends_on('py-pkgconfig')
    depends_on('py-pox')
    depends_on('py-ppft')
    #pprof: Added to LCG in 2019.
    depends_on('py-prettytable')
    depends_on('py-prompt-toolkit')
    depends_on('protobuf')
    depends_on('py-psutil')
    depends_on('py-ptyprocess')
    depends_on('py-py')
    depends_on('py-py4j')
    depends_on('py-pyasn1')
    depends_on('py-pyasn1-modules')
    depends_on('py-pycodestyle')
    depends_on('py-pycparser')
    depends_on('py-pyflakes')
    depends_on('py-pygments')
    depends_on('py-pyheadtail')
    depends_on('py-json5')
    depends_on('py-pylint')
    depends_on('py-pynacl')
    depends_on('py-pyparsing')
    depends_on('py-pyqt5')
    #PyRDF 	0.2.1 # TODO: py-rdflib?
    depends_on('py-pytest')
    # python:  add ~debug to make sure gdb does not enable debug builds
    # to avoid possible overhead
    depends_on('python ~debug') 
    depends_on('py-python-dateutil')
    depends_on('py-python-gitlab')
    #pytimber 	2.7.0
    depends_on('py-pytz')
    depends_on('py-pywavelets')
    depends_on('py-pyyaml')
    depends_on('py-pyzmq')
    depends_on('py-qmtest')
    depends_on('py-qtconsole')
    depends_on('py-qtpy')
    depends_on('r')
    depends_on('range-v3')
    depends_on('readline')
    depends_on('relax')
    depends_on('py-requests')
    depends_on('py-requests-oauthlib')
    depends_on('py-retrying')
    depends_on('root')
    depends_on('py-rsa')
    depends_on('py-scikit-image')
    depends_on('py-scipy')
    depends_on('py-setuptools')
    depends_on('py-setuptools-scm')
    depends_on('py-simplegeneric')
    depends_on('py-singledispatch')
    depends_on('py-sip')
    depends_on('py-six')
    depends_on('py-snappy')
    depends_on('sollya')
    depends_on('py-sortedcontainers')
    depends_on('spark')
    depends_on('spdlog')
    depends_on('py-stomp-py')
    depends_on('storm')
    depends_on('swig')
    depends_on('intel-tbb')
    depends_on('py-tblib')
    depends_on('py-terminado')
    depends_on('py-testpath')
    depends_on('py-toolz')
    depends_on('py-tornado')
    depends_on('py-traitlets')
    depends_on('py-unittest2')
    depends_on('py-uproot')
    depends_on('py-urllib3')
    depends_on('valgrind   ~ubsan')
    depends_on('vdt')
    depends_on('vectorclass-version2')
    depends_on('py-wcwidth')
    depends_on('py-websocket-client')
    depends_on('py-wheel')
    depends_on('py-widgetsnbextension')
    depends_on('xapian-core')
    depends_on('yaml-cpp')
    depends_on('libzmq')
    depends_on('cppzmq')
    depends_on('py-zict')
    depends_on('zlib')

    ###       Other
    depends_on('py-absl-py')
    depends_on(' acts')
    depends_on('alpaka')
    depends_on('arrow')
    depends_on('assimp')
    depends_on('py-astunparse')
    depends_on('py-attrs')
    depends_on('py-backcall')
    depends_on('bison')
    depends_on('py-bottleneck')
    depends_on('catch2')
    depends_on('py-colorcet')
    depends_on('py-control')
    depends_on('py-cx-oracle')
    # py-dataclasses is a backport that is only needed for python 3.6
    #depends_on('py-dataclasses', when="^python@3.6.0:3.6.99")
    depends_on('dd4hep')
    depends_on('py-docopt')
    depends_on('flex')
    depends_on('py-funcsigs')
    depends_on('py-gast')
    depends_on('gaudi')
    depends_on('py-genshi')
    depends_on('gettext')
    depends_on('git')
    depends_on('py-globus-sdk')
    depends_on('go')
    depends_on('py-google-auth-oauthlib')
    depends_on('py-google-pasta')
    # gophernotes: Added to LCG in 2019.
    # go_readline: Added to LCG in 2019. Source last updated in 2018: https://github.com/chzyer/readline
    # go_runewidth: Added to LCG in 2019.
    # go.uuid: Added to LCG in 2019.
    # go_zmq4: Added to LCG in 2019.
    depends_on('py-grpcio')
    depends_on('hadoop-xrootd', when='%gcc')
    depends_on('highfive')
    depends_on('py-hypothesis')
    depends_on('py-iminuit')
    depends_on('py-importlib-metadata')
    depends_on('py-importlib-resources')
    depends_on('py-iniconfig')
    # ipydatawidgets 	4.0.1
    #depends_on('py-ipympl') # disabled due to issues with nodejs
    depends_on('py-ipyparallel')
    depends_on('py-isort')
    depends_on(' itk')
    # itk_core 	5.1.1
    # itk_filtering 	5.1.1
    # itk_io 	5.1.1
    # itk_meshtopolydata 	0.6.2
    # itk_numerics 	5.1.1
    # itk_registration 	5.1.1
    # itk_segmentation 	5.1.1
    # itkwidgets 	0.32.0
    depends_on('py-joblib')
    depends_on('py-jupyter')
    depends_on('py-jupyter-client')
    depends_on('py-jupyter-console')
    depends_on('py-jupyter-core')
    # jupyter_highlight_selected_word 	0.2.0
    depends_on('py-jupyterlab')
    depends_on('py-jupyterlab-server')
    # jupyter_latex_envs 	1.4.4
    depends_on('py-lazy-object-proxy')
    depends_on('libffi')
    depends_on('libgeotiff')
    # libpqxx 	7.1.1
    depends_on('libxkbcommon')
    #depends_on('py-llvmlite') # disabled to avoid huge llvm dependencies
    depends_on('log4cpp')
    # m2crypto 	0.34.0
    depends_on('py-markdown')
    depends_on('py-mccabe')
    depends_on('py-more-itertools')
    depends_on('py-mpmath')
    depends_on('msgpack-c')
    depends_on('py-mysql-connector-python')
    depends_on('nanomsg')
    depends_on('py-nbconvert')
    depends_on('py-numba')
    depends_on(' octave')
    # octavekernel 	0.31.0
    # omniorb 	4.2.2
    depends_on('openmpi')
    depends_on('py-opt-einsum')
    depends_on('py-owslib')
    depends_on('py-packaging')
    depends_on('pacparser')
    depends_on('py-param')
    depends_on('py-paramiko')
    depends_on('py-parsl')
    depends_on('py-parso')
    depends_on('py-patsy')
    depends_on('py-pbr')
    depends_on('py-pillow')
    depends_on('py-plotly')
    depends_on('py-pluggy')
    # prctl 	1.7
    depends_on('py-prometheus-client')
    depends_on('py-pyarrow')
    depends_on('py-pybind11')
    depends_on('py-pyjwt')
    depends_on('py-pyrsistent')
    depends_on('py-pyserial')
    depends_on('py-pytest-runner')
    depends_on('py-pythonsollya')
    depends_on('py-pytools')
    depends_on('rapidjson')
    depends_on('py-redis')
    depends_on('py-rise')
    depends_on('py-scandir')
    depends_on('py-send2trash')
    depends_on('py-shapely')
    depends_on('py-simplejson')
    depends_on('py-sqlalchemy')
    depends_on('py-statsmodels')
    depends_on('py-termcolor')
    depends_on('texinfo')
    depends_on('thrift')
    depends_on('py-toml')
    depends_on('py-typed-ast')
    depends_on('py-typeguard')
    depends_on('py-typing-extensions')
    depends_on('py-tzlocal')
    depends_on('umesimd')
    depends_on('py-uncertainties')
    depends_on('py-uproot3-methods')
    depends_on('py-vcversioner')
    depends_on('veccore')
    depends_on('py-webencodings')
    depends_on('py-werkzeug')
    depends_on('py-wrapt')
    depends_on('py-zipp')
    depends_on('zstd')
    depends_on('kokkos')
    depends_on('sycl')
    # depends_on('julia') Forces openblas+ilp64 which causes octave error:
    # https://gitlab.cern.ch/sft/sft-spack-repo/-/jobs/20058551#L1010


    ### Generator
    depends_on('agile')
    depends_on('alpgen')
    #ampt 	2.26t9b_atlas
    depends_on('baurmc')
    depends_on('chaplin')
    depends_on('collier')
    depends_on('crmc')
    depends_on('evtgen')
    depends_on('feynhiggs')
    depends_on('form')
    depends_on('garfieldpp')
    depends_on('py-gosam')
    depends_on('gosam-contrib')
    depends_on('hepmcanalysis')
    depends_on('heputils')
    depends_on('herwig3')
    #hijing 	1.383bs.2
    depends_on('hoppet')
    depends_on('hto4l')
    #hydjet 	1.8
    #hydjet++ 	2.1
    #jhu 	5.6.3
    depends_on('lhapdf')
    depends_on('looptools')
    depends_on('madgraph5amc')
    #mctester 	1.25.0
    depends_on('mcutils')
    depends_on('njet')
    depends_on('openloops')
    depends_on('photos')
    #photos++ 	3.56.lhcb1
    #powheg-box-v2 	r3744.lhcb2.rdynamic
    depends_on('professor')
    depends_on('prophecy4f')
    #pyquen 	1.5.1
    depends_on('pythia6')
    depends_on('pythia8')
    depends_on('qd')
    depends_on('qgraf')
    #rapidsim 	1.4.4
    depends_on('recola')
    depends_on('recola-sm')
    depends_on('rivet')
    depends_on('sherpa')
    depends_on('starlight')
    depends_on('superchic')
    depends_on('syscalc')
    depends_on('tauola')
    #tauola++ 	1.1.6b.lhcb
    depends_on('thepeg')
    depends_on('vbfnlo')
    depends_on('yoda')

    ### Machine Learning
    # C50 	2.07
    # catboost 	0.9.1.1
    depends_on('py-keras')
    depends_on('py-keras-applications')
    depends_on('py-keras-preprocessing')
    depends_on('lwtnn')
    # neurobayes 	3.7.0
    # neurobayes_expert 	3.7.0
    depends_on('py-pystan')
    depends_on('py-scikit-learn')
    depends_on('py-tensorboard')
    depends_on('py-tensorflow')
    depends_on('py-tensorflow-estimator')
    depends_on('py-tensorflow-probability')
    depends_on('py-tensorboard-plugin-wit')
    depends_on('py-theano')
    depends_on('py-torch')
    depends_on('py-torchvision')
    depends_on('xgboost')
