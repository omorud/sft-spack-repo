# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *
import sys

class Libtensorflow(Package):
    """C support for tensorflow"""

    homepage = "https://www.tensorflow.org/install/lang_c"
    if sys.platform == 'darwin':
        version('2.8.0', sha256='139de721dc6965f3b0cc25990e5592fa54f4f8f6006bb4447ab63bc0066399ce',
                url="https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-darwin-x86_64-2.8.0.tar.gz")
    elif sys.platform.startswith('linux'):
        version('2.8.0', sha256='31918af292ea45ecb7ee4682d5991ca4f20bf7d3d053ee8e92f7467be1420e67',
                url="https://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-cpu-linux-x86_64-2.8.0.tar.gz")
    def install(self, spec, prefix):
        mkdir(prefix.lib)
        mkdir(prefix.include)
        install_tree('./lib', prefix.lib)
        install_tree('./include', prefix.include)
